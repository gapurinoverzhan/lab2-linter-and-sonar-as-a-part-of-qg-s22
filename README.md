# Lab 2 -- Linter and SonarQube as a part of quality gates


## Introduction

When talking about quality in projects, we usually think about testing, code reviews and coding conventions, but we are usually forgetting about the first layer of defense against bugs and bad code - static analysis. It is usually done by compiler/interpreter when you are building your application, but there are some advanced tools like linter and SonarQube as well, that allow to prevent bugs even without launching your code. ***Let's roll!***

## Linter

I am sure that those of you who have already worked with JS already used linter, because it is a standard almost in any enterprise project. It allows to check code for following some parts of the codestyle and catch possible bugs before launching the product. Good way to use linter is to run it locally when developer is trying to commit his code, this way he even won't be able to send incorrect code to external repo.

## SonarQube

It is very interesting tool to use, maybe not that popular, but there is a reason why we are talking about it on this course(no, nobody from the university staff is working there). It allows to statically analyse software, with support of huge variety of the programming languages. It can easily detect potential bugs, security vulnerabilities, technical debt and it does all of these automatically. It is a really nice tool to use for checking your code before pushing to the production. 

## Lab

I hope that you remember the previous lab. Today we are going to do something a little more interesting with our CI, we are going to add statical analysis as a few steps in our CI and analyse our code with SonarQube for different faiures(using CI as well).
1. Create your branch of the `
Lab2 - Linter and Sonar as a part of QG
` repo. [***Here***](https://gitlab.com/sqr-inno/lab2-linter-and-sonar-as-a-part-of-qg-s22)
2. Download and install Community version of SonarQube or run its docker image. [***Here***](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/).
**`IMPORTANT. To run SonarQube under Linux you need to install elasticSearch first, you may dowload it`** [here](https://discuss.elastic.co/t/elasticsearch-yml-configuration/198506/3). **` Also if you are running Linux, go to the -sonarqube_dir-/conf and change web.port and search.port to 8080 and 8081 accordingly(it won't work with default ports.)`**
3. Let's simply analyse our code at first. Open SonarQube. [***If any issues look here***](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/)
- At first, open [http://127.0.0.1:9000] or [http://127.0.0.1:8080] depending if you've changed your config file or not. Login: `admin`, Password: `admin`
- Then, click `Create new project` in the middle of the webpage, name it lab2_your_surname(both in key and display name)
- Create your token, and pick Maven as your technology.
- Open your cloned branch of the repo, and command that appeared in your browser. It should look something like this:
```
mvn sonar:sonar \
  -Dsonar.projectKey=your_key \
  -Dsonar.host.url=your_url \
  -Dsonar.login=your_token
```
After build you should recieve results of your analysis, and be able to see it in your branch.

4. If you are using anything other then JS in your project doesn't mean that you don't need linter. Keeping your code as clean as possible will save you a lot of your own nerves later on. Let's add linter to our Java project. Open `pom.xml` and add this to the plugins:
```xml
<plugin>
  <groupId>com.github.spotbugs</groupId>
  <artifactId>spotbugs-maven-plugin</artifactId>
  <version>4.5.2.0</version>
  <dependencies>
    <!-- overwrite dependency on spotbugs if you want to specify the version of spotbugs -->
    <dependency>
      <groupId>com.github.spotbugs</groupId>
      <artifactId>spotbugs</artifactId>
      <version>4.5.3</version>
    </dependency>
  </dependencies>
</plugin>
```
This is a linter for java that allows to check for bugs statically, now let's open .gitlab-ci.yml and add our linter as one of the steps:
```yml
linter:
  stage: linter
  image: maven
  script:
    - chmod +x mvnw
    - ./mvnw spotbugs:spotbugs
  dependencies:
  - build

```
**Don't forget to add linter to the stages on the top of the yml!** And enjoy your green pipeline :)
## Homework

As a homework you will need to add automatical SonarCloud check to your CI. SonarCloud is a simplified version of SonarQube. [you can find it here](https://sonarcloud.io). Once again, lab should consist all of the checks, for this lab you may mark your SonarCloud check with `allow_failure: true`, Your resulting pipeline should have three steps: `build` -> `linter` -> `sonar_cloud`. As well you should attach screenshots of SonarQube check to your readme file.
**Lab is counted as done, if your pipelines are passing.**

![Alt text](./screenshots/1.png?raw=true "Optional Title")
![Alt text](./screenshots/2.png?raw=true "Optional Title")
![Alt text](./screenshots/3.png?raw=true "Optional Title")
![Alt text](./screenshots/4.png?raw=true "Optional Title")
![Alt text](./screenshots/5.png?raw=true "Optional Title")
![Alt text](./screenshots/6.png?raw=true "Optional Title")
![Alt text](./screenshots/7.png?raw=true "Optional Title")
![Alt text](./screenshots/8.png?raw=true "Optional Title")
